# The Lighthouse Service

This service acts an acronym dictionary of sorts. You provide an acronym and the 
service tells you some bits of information about it, 

## Building
    
    go fmt *.go && go build
    
## Running

    DIGME_DATABASE_VENDOR=sqlite3 \
    DIGME_DATABASE_NAME=lighthouse \
    DIGME_MIGRATION_PATH=./migrations \
    go run *.go


## Usage

### Authentication

This service supports basic auth. You can enable basic auth by defining a 
`AUTHENTICATED` environment variable and setting it to true `true`. Users 
are read from the _users_ table. In debug mode, credentials are added for you.

See an example authenticated request below:

    curl --user "lighthouse-keeper@yahooo.com:I <3 wickies!" 127.0.0.1:8080/acronyms/U.N.


### Adding an acronym

    curl -X POST "127.0.0.1:8080/acronyms" \ 
    --data "@test/example-payload.json" \ 
    -H "Content-Type: application/json" \ 
    -H "Accept: application/json" -v

     // Returns
        {
        	"acronym": "U.N.",
        	"expanded": "United Nations",
        	"description": "The United Nations (UN) is an intergovernmental organization tasked to promote international co-operation and to create and maintain international order"
        }


### Finding an acronym

    curl 127.0.0.1:8080/acronyms/U.N.

    // Returns
    {
    	"acronym": "U.N.",
    	"expanded": "United Nations",
    	"description": "The United Nations (UN) is an intergovernmental organization tasked to promote international co-operation and to create and maintain international order"
    }

### Getting all acronyms
    
    curl "127.0.0.1:8080/acronyms"
    // Returns
    {
    	"content": [{
    		"acronym": "U.N.",
    		"expanded": "United Nations",
    		"description": "The United Nations (UN) is an intergovernmental organization tasked to promote international co-operation and to create and maintain international order"
    	}]
    }
