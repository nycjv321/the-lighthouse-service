package main

import (
	"os"
	"strings"
)

func authenticateRequests() bool {
	return strings.Compare(os.Getenv("AUTHENTICATED"), "true") == 0
}
