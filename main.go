package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.nycjv321.com/digme"
	"gitlab.com/nycjv321/bactrian"
)

func init() {
	digme.Initialize()
}

func acronymRouter(router *gin.Engine) (*gin.RouterGroup) {
	return router.Group("/acronyms")
}

func getEngine() (*gin.Engine) {
	return gin.Default()
}

func authenticated(engine *gin.Engine) *gin.Engine{
	if authenticateRequests() {
		engine.Use(bactrian.BasicAuth(users))
	}
	return engine
}

func main() {
	router := authenticated(getEngine())
	requests := acronymRouter(router)
	requests.GET("", getAcronyms)
	requests.GET("/:acronym", getAcronym)
	requests.POST("", createAcronym)
	router.Run()
}
