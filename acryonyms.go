package main

import (
	"github.com/gocraft/dbr"
)

type Acronyms struct {
	Content []Acronym `json:"content"`
}

type Acronym struct {
	Acronym     string `json:"acronym"`
	Expanded    string `json:"expanded"`
	Description string `json:"description"`
}

type AcronymService struct{}

var connection, _ = dbr.Open("sqlite3", "lighthouse.db", nil)

func selectAcronyms(session *dbr.Session) *dbr.SelectBuilder{
	return session.Select("acronym", "expanded", "description").From("acronyms")
}

func (AcronymService) All() (*Acronyms, error) {
	session := connection.NewSession(nil)
	acronyms := []Acronym{}
	_, err := selectAcronyms(session).LoadValues(&acronyms)
	return &Acronyms{acronyms}, err
}

func (service AcronymService) Get(name string) (*Acronym, error) {
	session := connection.NewSession(nil)
	acronym := Acronym{}
	_, err := selectAcronyms(session).Where("acronym like ?", name).Load(&acronym)
	return &acronym, err
}

func (service AcronymService) Add(acronym *Acronym) (*Acronym, error) {
	session := connection.NewSession(nil)
	_, err := session.InsertInto("acronyms").Columns("acronym", "expanded", "description").Record(acronym).Exec()
	return acronym, err
}
