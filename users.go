package main

import (
	"os"
	"strings"
	"gitlab.com/nycjv321/bactrian"
	"golang.org/x/crypto/bcrypt"
	"github.com/gocraft/dbr"
)


type AuthenticatedUserService struct {
	bactrian.UserService
}

var dummyUser = bactrian.User{Identity:"lighthouse-keeper@yahooo.com", Authentication: bactrian.Hash("I <3 wickies!")}

func isDebug() bool{
	return strings.Compare(os.Getenv("GIN_MODE"), "debug")  == 0 ||  len(os.Getenv("GIN_MODE")) == 0
}

func (users AuthenticatedUserService) Authenticates(user bactrian.User, password string) bool{
	return bcrypt.CompareHashAndPassword([]byte(user.Authentication), []byte(password)) == nil
}

func (AuthenticatedUserService) Get(email string) (bactrian.User, error) {
	session := connection.NewSession(nil)
	if isDebug() {
		return dummyUser, nil
	} else {
		user := bactrian.User{}
		if _, err := selectUsers(session).Where("email like ?", email).Load(&user); err == nil {
			return user, err
		} else {
			return bactrian.User{}, err
		}
	}
}

func selectUsers(session *dbr.Session) *dbr.SelectBuilder{
	return session.Select("email", "password").From("users")
}
