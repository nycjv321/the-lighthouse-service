package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
)

var acronyms = AcronymService{}
var users = AuthenticatedUserService{}

func getAcronyms(c *gin.Context) {
		if result, err := acronyms.All(); err == nil {
			c.JSON(http.StatusOK, result)
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		}
}

func getAcronym(c *gin.Context) {
	name := c.Param("acronym")
	if acronym, err := acronyms.Get(name); err == nil {
		if len(acronym.Acronym) == 0 {
			c.JSON(http.StatusNotFound, gin.H{"message": fmt.Sprintf("'%v' found an existing acronym. Go ahead and add it. Contribute!", name)})

		} else {
			c.JSON(http.StatusOK, acronym)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
}

func createAcronym(c *gin.Context) {
	var acronym Acronym
	if err := c.BindJSON(&acronym); err == nil {
		if result, err := acronyms.Add(&acronym); err == nil {
			c.JSON(http.StatusOK, result)
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"message": "Woah... What are you trying to do there buddy?"})
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
}